.. MyCode documentation master file, created by
   sphinx-quickstart on Wed Mar 16 09:46:56 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to QuNex's Google formatted documentation!
==================================================


.. automodule:: workflow_google
   :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
