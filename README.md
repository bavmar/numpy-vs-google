# Comparison of NumPy and Google Style Docstrings

[[_TOC_]]


## Overview

To allow for consistent automatic generation of QuNex online documentation, standardization upon a single docstring style is required. Sphinx document generator can generate such documentation from inline code using the [Napoleon extension](https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html). The extension accepts two similar docstring styles: [NumPy](https://numpydoc.readthedocs.io/en/latest/format.html) and [Google](https://google.github.io/styleguide/pyguide.html#s3.8-comments-and-docstrings).

NumPy style makes use of predefined section headings followed by short dashes (------), while Google style makes use of headings preceded with indentations of 4 spaces and ending with a colon. NumPy by default does not allow for custom subsections such as the ones used in longer docstrings of QuNex inline code. Google allows for any number of custom subsections with the use of indentations which result in a slightly narrower subsection width.

Code used throughout this document can be obtained from https://gitlab.com/bavmar/numpy-vs-google. Python source files are located in the '_src_' folder, while the generated HTML documentation is located in '__build/html_'.


## Online documentation

Online documentation was rendered with Sphinx using two extensions: _sphinx.ext.autodoc_ and _sphinx.ext.napoleon_. The current NumPy project theme _pydata_sphinx_theme_ was selected. 

### Sections

The Napoleon extension allows for a limited set of section headings. By default only the following are allowed [[_source_]](https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html#docstring-sections):

* **Args** (alias of _Parameters_)
* **Arguments** (alias of _Parameters_)
* **Attributes**
* **Example**
* **Examples**
* **Keyword Args** (alias of _Keyword Arguments_)
* **Keyword Arguments**
* **Methods**
* **Note**
* **Notes**
* **Other Parameters**
* **Parameters**
* **Return** (alias of _Returns_)
* **Returns**
* **Raises**
* **References**
* **See Also**
* **Warning**
* **Warnings** (alias of _Warning_)
* **Warns**
* **Yield** (alias of _Yields_)
* **Yields**

Certain existing sections will have to be renamed or/and moved according to Napoleon guidelines:

* USE → extended summary (at the top) or Notes section
* INPUTS → Parameters
* General parameters → Parameters
* Specific parametes → Other Parameters
* EXAMPLE USE → Examples

Use of all capitals in section headings makes sense when viewing docstrings in terminal, less so when viewing them online. Niether NumPy nor Google styles use all capital headings so these should be avoided.

#### Subsections

An issue arises when trying to structure subsections within the Notes section using NumPy. An example of a docstring section formatted strictly according to NumPy guidelines can be found below:

```
Notes
-----
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ac auctor augue mauris
augue neque gravida. In fermentum posuere urna nec tincidunt praesent
semper feugiat. Erat imperdiet sed euismod nisi porta. Ut tellus elementum
sagittis vitae et.

**Subsection**

Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque.
Sit amet nisl purus in mollis nunc sed id semper. Sapien et ligula
ullamcorper malesuada proin libero nunc consequat. Lacus vel facilisis
volutpat est velit egestas dui. Sed libero enim sed faucibus turpis in eu.
Montes nascetur ridiculus mus mauris vitae. Vulputate mi sit amet mauris
commodo quis.

*Subsubsection*

In ante metus dictum at tempor commodo ullamcorper a lacus. Natoque
penatibus et magnis dis parturient montes nascetur ridiculus. Nulla
aliquet porttitor lacus luctus accumsan tortor posuere ac. Nisi quis
eleifend quam adipiscing vitae proin.

Imperdiet sed euismod nisi porta lorem mollis aliquam.

A `arcu cursus`:

- vitae
- congue
- mauris
- rhoncus
- aenean vel.

Et ligula ullamcorper malesuada proin libero nunc consequat.

**Subsection**

Vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt. Cras
sed felis eget velit aliquet sagittis id consectetur purus. Tincidunt
vitae semper quis lectus nulla at. Sed augue lacus viverra vitae.
```

![](img/online-subsections-numpy-default.png)

The structure of the above output is not immediately apparent since subsection headings are marked only using bold text and subsubsections are marked using italic text, both of which can be used in contexts other than headings.

The same string formatted according to Google guidelines can be found below:

```
Notes:
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ac auctor augue mauris
    augue neque gravida. In fermentum posuere urna nec tincidunt praesent
    semper feugiat. Erat imperdiet sed euismod nisi porta. Ut tellus elementum
    sagittis vitae et.
    
    Subsection:
        Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque.
        Sit amet nisl purus in mollis nunc sed id semper. Sapien et ligula
        ullamcorper malesuada proin libero nunc consequat. Lacus vel facilisis
        volutpat est velit egestas dui. Sed libero enim sed faucibus turpis in eu.
        Montes nascetur ridiculus mus mauris vitae. Vulputate mi sit amet mauris
        commodo quis.

        Subsubsection:
            In ante metus dictum at tempor commodo ullamcorper a lacus. Natoque
            penatibus et magnis dis parturient montes nascetur ridiculus. Nulla
            aliquet porttitor lacus luctus accumsan tortor posuere ac. Nisi quis
            eleifend quam adipiscing vitae proin.
        
            Imperdiet sed euismod nisi porta lorem mollis aliquam.
            
            A `arcu cursus`:
            
            - vitae
            - congue
            - mauris
            - rhoncus
            - aenean vel.
            
            Et ligula ullamcorper malesuada proin libero nunc consequat.

    Subsection:
        Vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt. Cras
        sed felis eget velit aliquet sagittis id consectetur purus. Tincidunt
        vitae semper quis lectus nulla at. Sed augue lacus viverra vitae.
```

![](img/online-subsections-google.png)

Use of indentations of 4 spaces make subsections immediately distinguishable from subsubsections. A colon must be added at the end of each heading. No additional line breaks between headings and section text are allowed.

##### Custom sections

Custom section headings can be manually added to the Sphinx _conf.py_ file using the _napoleon_custom_sections_ variable:

```python
# common sections in QuNex
napoleon_custom_sections = [
    "inputs",
    ("general parameters", "params_style"),
    ("other parameters", "params_style"),
    "use",
    "example use"
]

# uncommon sections
napoleon_custom_sections.extend([
    "scrubbing",
    "spatial smoothing",
    "temporal filtering",
    "regression"
])
```

This can be particularly useful with NumPy formatted docstrings as it enables the use of an additional style (level) of heading with a horizontal line. In our example the use of italic text as a way to mark subsubsection headings is therefore not required and bold text can be used instead.

```
Notes
-----
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ac auctor augue mauris
augue neque gravida. In fermentum posuere urna nec tincidunt praesent
semper feugiat. Erat imperdiet sed euismod nisi porta. Ut tellus elementum
sagittis vitae et.

Subsection
----------
Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque.
Sit amet nisl purus in mollis nunc sed id semper. Sapien et ligula
ullamcorper malesuada proin libero nunc consequat. Lacus vel facilisis
volutpat est velit egestas dui. Sed libero enim sed faucibus turpis in eu.
Montes nascetur ridiculus mus mauris vitae. Vulputate mi sit amet mauris
commodo quis.

**Subsubsection**

In ante metus dictum at tempor commodo ullamcorper a lacus. Natoque
penatibus et magnis dis parturient montes nascetur ridiculus. Nulla
aliquet porttitor lacus luctus accumsan tortor posuere ac. Nisi quis
eleifend quam adipiscing vitae proin.

Imperdiet sed euismod nisi porta lorem mollis aliquam.

A `arcu cursus`:

- vitae
- congue
- mauris
- rhoncus
- aenean vel.

Et ligula ullamcorper malesuada proin libero nunc consequat.

Subsection
----------
Vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt. Cras
sed felis eget velit aliquet sagittis id consectetur purus. Tincidunt
vitae semper quis lectus nulla at. Sed augue lacus viverra vitae.
```

![](img/online-subsections-numpy-custom.png)

This practice can alleviate some shortcomings of the NumPy style but is time-consuming and cumbersome in the long run. It also strays from standardization which is crucial for good consistent documentation.

### Beginning sections and Examples

Beginning sections of a function's docstring (short summary, extended summary, Parameters) and Examples are rendered identically using either NumPy or Google style.

Beginning of the _workflow.preprocess_conc_ function's docstring:

![Online beginning](img/online-beginning.png)

The Parameters section can only contain a list of parameters and corresponding details. No additional paragraphs between listed parameters are allowed. Some such existing paragraphs in QuNex docstrings will have to be moved to the Notes section.

Examples section:

![Online Examples](img/online-examples.png)

### Notes section

Show below are two screenshots of the _Spatial smoothing_ subsection in the _Notes_ section of the _workflow.preprocess_conc_ function. The _Spatial smoothing_ subsection is relatively long and uses various levels of headings so it serves as a good comparison of NumPy and Google docstrings.

NumPy style:

![](img/online-notes-numpy.png)

In the example above extra colons were added to _Volume smoothing_, _Cifti smoothing_ and _Results_ headings to make their role more apparent.

Google style:

![](img/online-notes-google.png)

For inline docstrings used to generate screenshots above see code examples in _Inline documentation - Notes section_ below.


## Inline documentation

Shown and discussed below are examples of inline docstrings from the Python _workflow.preprocess\_conc_ function using current, NumPy and Google styles.

### Parameters section

Both styles ignore sections headings followed by equal signs (====, e.g. INPUTS) so these need to either be converted to dashes or removed in favor of predefined headings (e.g. Parameters) dictated by the Napoleon extension.

Current style:

```
⋮

INPUTS
======

General parameters
------------------

The function takes the usual general processing parameters:

--sessions            The batch.txt file with all the session information
                      [batch.txt].
--sessionsfolder      The path to the study/sessions folder, where the
                      imaging  data is supposed to go [.].

⋮
```

NumPy style:

```
⋮

Parameters
----------
--sessions : str, optional, default 'batch.txt'
    The batch.txt file with all the session information.
--sessionsfolder : str, optional, default '.'
    The path to the study/sessions folder, where the
    imaging  data is supposed to go.
--parsessions : int, optional, default 1
    How many sessions to run in parallel.

⋮
```

Google style:

```
⋮

Parameters:
    --sessions (str, optional, default 'batch.txt'):
        The batch.txt file with all the session information.
    --sessionsfolder (str, optional, default '.'):
        The path to the study/sessions folder, where the
        imaging  data is supposed to go.
    --parsessions (int, optional, default 1):
        How many sessions to run in parallel.

⋮
```

Google recommends that parameters are detailed in a single line. This can result in long lines of text which are hard to read in the terminal, so in the example above line breaks were used after the colon of each listed parameter. Online documentation is rendered identically in each case.

### Notes section

Current style:

```
⋮

SPATIAL SMOOTHING
=================

Volume smoothing
----------------

For volume formats the images will be smoothed using the img_smooth_3d
nimage method. For cifti format the smooting will be done by calling the
relevant wb_command command. The smoothing specific parameters are:

--voxel_smooth      Gaussian smoothing FWHM in voxels [2]
--smooth_mask       Whether to smooth only within a mask, and what mask to
                    use (nonzero/brainsignal/brainmask/<filename>)[false].
--dilate_mask       Whether to dilate the image after masked smoothing and
                    what mask to use (nonzero/brainsignal/brainmask/
                    same/<filename>)[false].

If a smoothing mask is set, only the signal within the specified mask will
be used in the smoothing. If a dilation mask is set, after smoothing within
a mask, the resulting signal will be constrained / dilated to the specified
dilation mask.

⋮
```

NumPy style:

```
⋮

Notes
-----

⋮

**Spatial smoothing**

*Volume smoothing:*

For volume formats the images will be smoothed using the img_smooth_3d
nimage method. For cifti format the smooting will be done by calling the
relevant wb_command command. The smoothing specific parameters are:

--voxel_smooth      Gaussian smoothing FWHM in voxels [2]
--smooth_mask       Whether to smooth only within a mask, and what mask to
                    use (nonzero/brainsignal/brainmask/<filename>)[false].
--dilate_mask       Whether to dilate the image after masked smoothing and
                    what mask to use (nonzero/brainsignal/brainmask/
                    same/<filename>)[false].

If a smoothing mask is set, only the signal within the specified mask will
be used in the smoothing. If a dilation mask is set, after smoothing within
a mask, the resulting signal will be constrained / dilated to the specified
dilation mask.

⋮
```

Google style:

```
⋮

Notes:

⋮

    Spatial smoothing:
        Volume smoothing:
            For volume formats the images will be smoothed using the img_smooth_3d
            nimage method. For cifti format the smooting will be done by calling the
            relevant wb_command command. The smoothing specific parameters are:

            --voxel_smooth      Gaussian smoothing FWHM in voxels [2]
            --smooth_mask       Whether to smooth only within a mask, and what mask to
                                use (nonzero/brainsignal/brainmask/<filename>)[false].
            --dilate_mask       Whether to dilate the image after masked smoothing and
                                what mask to use (nonzero/brainsignal/brainmask/
                                same/<filename>)[false].

            If a smoothing mask is set, only the signal within the specified mask will
            be used in the smoothing. If a dilation mask is set, after smoothing within
            a mask, the resulting signal will be constrained / dilated to the specified
            dilation mask.

⋮
```


## Summary

For QuNex project docstrings I recommend the following order of sections:

1. Short summary
2. Extended summary
3. **Parameters**
4. **Other Parameters**
5. **Notes**
6. **Examples**

Use of Google style docstring format is recommended as it is easier to write and read online. The only considerable disadvantages when compared to NumPy style are narrower subsections because of the use of indentations and vertically more condensed headings when viewed in the terminal.


*Written by Martin Bavčar, 2022-03-31*
