#!/usr/bin/env python
# encoding: utf-8

"""
heading examples module
"""

def heading_examples():
    """
    Short summary example
    
    Notes
    -----
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ac auctor augue mauris
    augue neque gravida. In fermentum posuere urna nec tincidunt praesent
    semper feugiat. Erat imperdiet sed euismod nisi porta. Ut tellus elementum
    sagittis vitae et.

    Subsection
    ----------
    Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque.
    Sit amet nisl purus in mollis nunc sed id semper. Sapien et ligula
    ullamcorper malesuada proin libero nunc consequat. Lacus vel facilisis
    volutpat est velit egestas dui. Sed libero enim sed faucibus turpis in eu.
    Montes nascetur ridiculus mus mauris vitae. Vulputate mi sit amet mauris
    commodo quis.

    **Subsubsection**

    In ante metus dictum at tempor commodo ullamcorper a lacus. Natoque
    penatibus et magnis dis parturient montes nascetur ridiculus. Nulla
    aliquet porttitor lacus luctus accumsan tortor posuere ac. Nisi quis
    eleifend quam adipiscing vitae proin.

    Imperdiet sed euismod nisi porta lorem mollis aliquam.

    A arcu cursus:

    - vitae
    - congue
    - mauris
    - rhoncus
    - aenean vel.

    Et ligula ullamcorper malesuada proin libero nunc consequat.

    Subsection
    ----------
    Vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt. Cras
    sed felis eget velit aliquet sagittis id consectetur purus. Tincidunt
    vitae semper quis lectus nulla at. Sed augue lacus viverra vitae.
    """
